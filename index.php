<?php
  $link = mysqli_connect("127.0.0.1", "root", "", "events_mconcepts");


 ?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <html xmlns="http://www.w3.org/1999/xhtml">
   <head>
     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
     <title>Event</title>
     <link rel="stylesheet" href="font-awesome.min.css"/>
     <script type="text/javascript" src="jquery-2.1.3.min.js"></script>
     <script type="text/javascript" src="jquery.slotmachine.min.js"></script>
    <script type="text/javascript" src="lodash.min.js"></script>
     <style type="text/css">
        @font-face {
          font-family: 'luckyfont';
          src: url('HelveticaNeue.ttf');
          font-weight: normal;
          font-style: normal;
        }

        @font-face {
          font-family: 'luckyfont2';
          src: url('HelveticaNeue.ttf');
          font-weight: normal;
          font-style: normal;
        }

        html, body {
          height: 100%;
          margin: 0px;
          background-color: #FFFFFF;
        }

        .hide {
          display: none;
        }

        .container
        {
          background-image: url("draw.jpg");
          background-size: cover;
          background-repeat: no-repeat;
          background-position: top center;
          height: 100%;
          font-size: 50px;
        }

        .barcode {
          /*background-color: rgba(255,255,255,0.7);*/
          margin:auto;
          position:absolute;
          top: 420px;
          width: 100%;
          height: 200px;
          overflow: hidden;
        }

        .slotMachine, .initDisplay{
          width: 100%;
          height: 300px;
          padding: 0px 50px;
          box-sizing: border-box;
          overflow: hidden;
          text-align: center;
          font-family: 'Arial';
          font-size: 100px;
          color: white;
          margin-right: 0px;
          margin-left: 0px;
          line-height: 180px;
        }

        #auxInfo {
          /*background-color:rgba(255,255,255,0.7);*/
          width: 100%;
          margin:auto;
          position:absolute;
          top: 650px;
          text-align: center;
          font-family: 'HelveticaNeue';
          font-size: 70px;
          line-height: 70px;
          color: black;
        }


        .status{
          position:absolute;
          top: 0px;
        }
    </style>
  </head>

   <body>
    <div class="container">

      <div class="barcode">
        <div class="initDisplay">----------------</div>
        <div id="machine1" class="slotMachine">
          <div id="theWinner" class="slot">&nbsp;</div>
          <div id="slot1" class="slot">asd</div>
          <div id="slot2" class="slot">asd</div>
          <div id="slot3" class="slot">asd</div>
          <div id="slot4" class="slot">asd</div>
          <div id="slot5" class="slot">asd</div>
          <div id="slot6" class="slot">asd</div>
        </div>
      </div>

      <div id="auxInfo" class="hide">
        <!-- <strong><span id="aux1span"></span></strong> -->
        <span id="aux2span"></span>
      </div>
    </div>

    <div id="status" class="status"></div>

     <script type="text/javascript" charset="utf-8">
      TIME = 1000;
      ROLLING_TIME = 1000;
      lastActionTime = 0;
      pauseState = 0;

      var contests = new Array();

      contests[0] = "test";

      var strID = "test"

      function haltRepeats(fn){
        var now = (new Date()).getTime();
        if(now >= lastActionTime + 1000)
        {
          lastActionTime = now;
          fn();
        }
      }

      function mode(list){
        return parseInt(_.chain(list).countBy().pairs().max(_.last).head().value(), 10);
      }

      /*
       * INSTRUCTIONS: getBaseHeight TRIES TO BE SMART BY INTERPRETING THE
       * ROW WITH THE SMALLEST HEIGHT, COMPARED TO slotMachine HEIGHT.
       */
      function getBaseHeight(){
        var heightList = [];

        $('.slot').each(function(){
          heightList.push($(this).height());
        });

        const minFieldHeight = _.min(heightList);
        const slotMachineHeight = $('.slotMachine').height();

        return Math.min(minFieldHeight, slotMachineHeight)
      }

      function resetSlotSizes(){
        const baseHeight = getBaseHeight();
        $('.slot').each(function(){
          var $slot = $(this);
          resetSingleSlotSize($slot, baseHeight);
        });
      }

      function resetSingleSlotSize($slot, baseHeight){
        // Reset font size
        $slot.attr('style', '');
        while($slot.height() > baseHeight){
          const currentFontSize = parseInt($slot.css('font-size'), 10);
          $slot.css('font-size', currentFontSize - 5);
        }
      }

      $(function(){
        resetSlotSizes();
        $(window).on('resize', resetSlotSizes);
        window.machine = $('#machine1').slotMachine({ active: 0, delay: 1000});
      });

      function generateWinner()
      {
        if(!window.isRunning)
        {
          //alert("Spin");
          window.isRunning = true;

          $(".initDisplay").hide();

          var intSlot = getRandomInt(0, 3);

          // Reset single slot size after setting the name so that the length
          // is accounted for, again.
          //$("#slot").html(data.name);

          $("#theWinner").html("hey");
          const baseHeight = getBaseHeight();
          resetSingleSlotSize($("#theWinner"), baseHeight);

          $("#slot0").html("SORRY TRY AGAIN!");
          $("#slot1").html("$50");
          $("#slot2").html("$150");
          $("#slot3").html("$50");
          $("#slot4").html("$100");
          $("#slot5").html("SORRY TRY AGAIN!");

          resetSingleSlotSize($("#slot0"), baseHeight);
          resetSingleSlotSize($("#slot1"), baseHeight);
          resetSingleSlotSize($("#slot2"), baseHeight);
          resetSingleSlotSize($("#slot3"), baseHeight);
          resetSingleSlotSize($("#slot4"), baseHeight);
          resetSingleSlotSize($("#slot5"), baseHeight);

          window.machine.setRandomize(0);
          window.machine.shuffle();
        }
        else
        {
          window.machine.stop();
          window.isRunning = false;

          setTimeout(function(){
            $("#auxInfo").delay(2500).fadeIn(TIME);
          }, 2000);

          $.post("ajax.php", { prize: "hey" }, function(data){
            alert(data);
          });
        }
      }


    </script>

    <script type="text/javascript" charset="utf-8">
      lastManualTime = 0;

      function manualTrigger(){
        var now = (new Date()).getTime();
        // Double Tap within 1 Second
        if(now < lastManualTime + 1000){
          haltRepeats(generateWinner);
          lastManualTime = 0;
        } else {
          lastManualTime = now;
        }
      }

      function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
      }

      document.addEventListener("keydown", function(e) {
        if (e.keyCode == 32){
          e.preventDefault();
          manualTrigger();
        }
      }, false);
     </script>

     <script type="text/javascript">
       document.body.style.cursor="none";
     </script>
   </body>
 </html>
